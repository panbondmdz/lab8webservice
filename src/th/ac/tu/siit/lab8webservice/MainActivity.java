package th.ac.tu.siit.lab8webservice;

import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String, String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	String province = "bangkok";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String, String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, new String[] {
				"name", "value" }, new int[] { R.id.tvName, R.id.tvValue });
		setListAdapter(adapter);
	}

	@Override
	protected void onStart() {
		super.onStart();
		ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			long current = System.currentTimeMillis();
			if (current - lastUpdate > 5 * 60 * 1000) {
				WeatherTask task = new WeatherTask(this);
				task.execute("http://cholwich.org/"+province+""+".json");
			}
		} else {
			Toast t = Toast
					.makeText(
							this,
							"No Internet Connectivity on this device. Check your setting",
							Toast.LENGTH_LONG);
			t.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if(outState != null){
			super.onSaveInstanceState(outState);
		}
		
		try {
			FileOutputStream outfile = openFileOutput("contact.tsv", MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			
			for(Map<String,String> m : list) {
				p.write(m.get("name")+"\t"+m.get("phone")+"\t"+m.get("type")+"\t"+m.get("email")+"\n");
			}
			p.flush(); p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.action_refresh:
		{
			ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				long current = System.currentTimeMillis();
				if (current - lastUpdate > 3 * 60 * 1000) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/"+province+""+".json");
				}
			} else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
			return true;
		}

			
		case R.id.action_bkk:{
			ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				long current = System.currentTimeMillis();
				if (current - lastUpdate > 0 * 60 * 1000) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/bangkok.json");
				}
			} else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}

			return true;
		}
		case R.id.action_ntb:{
			ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				long current = System.currentTimeMillis();
				if (current - lastUpdate > 0 * 60 * 1000) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/nonthaburi.json");
				}
			} else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
			return true;
		}
		case R.id.action_ptt:{
			
			ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				long current = System.currentTimeMillis();
				if (current - lastUpdate > 0 * 60 * 1000) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/pathumthani.json");
				}
			} else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
		}
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String, String> record;
		ProgressDialog dialog;

		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), result,
					Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			setTitle("Bangkok Weather");
		}

		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				URL url = new URL(params[0]);
				HttpURLConnection http = (HttpURLConnection) url
						.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				http.setDoInput(true);
				http.connect();

				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(
							http.getInputStream()));
					while ((line = in.readLine()) != null) {
						buffer.append(line);
					}
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String, String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp") - 273.0;
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", temp));
					list.add(record);
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);
					String description = w0.getString("description");
					record = new HashMap<String, String>();
					record.put("name", "Description");
					record.put("value", description);
					list.add(record);

					String pressure = jmain.getString("pressure");
					record = new HashMap<String, String>();
					record.put("name", "Pressure");
					record.put("value", pressure + "hPa");
					list.add(record);
					String humidity = jmain.getString("humidity");
					record = new HashMap<String, String>();
					record.put("name", "Humidity");
					record.put("value", humidity + "%");
					list.add(record);
					double tempMin = jmain.getDouble("temp_min") - 273;
					record = new HashMap<String, String>();
					record.put("name", "Temp Min");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", tempMin));
					list.add(record);
					double tempMax = jmain.getDouble("temp_max") - 273;
					record = new HashMap<String, String>();
					record.put("name", "Temp Max");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", tempMax));
					list.add(record);
					String windSpeed = json.getJSONObject("wind").getString(
							"speed");
					record = new HashMap<String, String>();
					record.put("name", "Wind Speed");
					record.put("value", windSpeed + "m/s");
					list.add(record);
					String windDeg = json.getJSONObject("wind")
							.getString("deg");
					record = new HashMap<String, String>();
					record.put("name", "Wind Deg");
					record.put("value", windDeg + "degree");
					list.add(record);

					return "Finished Loading Weather Data";
				} else {
					return "Error " + response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}
		}
	}

}
